import streamlit as st
import pandas as pd
import seaborn as sb
import matplotlib.pyplot as plt

st.markdown(
    '# Est ce que les statistiques de bases des champions de League of Legends représentent le rôle principal de celui ci')

st.markdown('# Consignes :')
st.markdown('### * Une problématique pertinente (c-a-d ou il est possible de répondre avec des données)')
st.markdown('### * Au moins 1 diagramme avec données continues, type nuage de point ou histogramme')
st.markdown('### * Au moins 2 diagrammes avec des données discrètes')
st.markdown('### * 1 boîte à moustaches avec filtrage des données aberrantes sur le dataset (si il y en a)')
st.markdown('### * 1 heat map avec matrice de corrélation (si pertinent)')
st.markdown('### * Des commentaires clairs et pertinents pour chaque graphiques')

st.markdown('## Importation des données')
df = pd.read_csv('LOL.csv')
df['Primary_Location'] = df['Primary_Location'].replace(to_replace=['bottom'], value=['Bottom'])

if st.checkbox('Voir le tableau'):
    st.dataframe(df.head())

st.markdown('## Première analyse')

option = st.selectbox(
    "Quel tableau souhaitez vous afficher ?",
    ('Poste sur la map', 'Rôle du champions en jeu')
)

if (option == "Poste sur la map"):
    sb.countplot(df, x='Primary_Location')
else:
    sb.countplot(df, x='Primary_Role')

st.pyplot(plt.gcf())

st.markdown('## Analyse des Rôles par Postes')

sb.displot(df, x='Primary_Location', multiple='stack', hue='Primary_Role', shrink=0.8)

st.pyplot(plt.gcf())

st.write('Ce graphique est très intéressant. Il répond à notre problématique du dessus : On distingue clairement un type de Rôle pour 3 des 5 Postes. Pour autant, des Combattants se retrouvent en Bottom, des Mages en Support et même des Supports en Top et en Jungle les rôles sont très diversifiés. On ne peut donc pas affirmer qu’un Rôle determine un Poste. Il nous faut donc analysés les statistiques des différents Rôles pour répondre à notre problèmatique : Les satitistiques de base d’un champion déterminent-elles son rôle dans le jeu ?')

st.markdown("## Analyse des statistiques")

st.markdown("### 1) La vie")

sb.boxplot(df, x='Primary_Role', y='Health')

st.pyplot(plt.gcf())

st.write('On peut voir que la répartition des points de vie (Health) par rapport au Rôle est assez inégale. Les Tanks et les Fighters ont généralement plus de vie que le reste et les Mages moins. Cependant, on peut voir que certains mages ont autant de vie que certains Tanks (qui sont censés être les plus les plus hauts). Il y a donc une tendance pour les Tank et les Fighter d’avir plus de vie mais ce graphique ne nous permet pas d’affirmer que cette statistique soit déterminante dans le rôle d’un champion.')

st.markdown('### 2) Armure')

sb.boxplot(df, x='Primary_Role', y='Armor')

st.pyplot(plt.gcf())

st.write('Cette fois si, on remarque que les Mages et les Marksmans ont en majorité moins d’armure que les autres. On remarque aussi que les Fighters, Tanks et Support ont tendance à être plus haut que les autres en armure. On peux donc conclure qu’un Mage aura forcément moins d’armure que les autres')

st.markdown('### 2) Armure')

sb.boxplot(df, x='Primary_Role', y='Range')

st.pyplot(plt.gcf())

st.write("Ici le graphique est très explicite. On remarque encore une fois que les Mages et les Marksmans sortent du lot.\n On peut donc répondre à notre problématique pour ces deux rôles. Oui, si un champion à plus de portée et moins d’amure que les autres. Il sera certainement un mage ou un tireur.\n On note aussi que les Supports ont aussi plus de portée que les autres rôles. Il reste maintenant à déterminer si d’autres statistiques influent sur d’autres postes.")

st.markdown('### 4) Le Mana')

sb.relplot(df, x='Primary_Role', y='Mana', kind='line')

st.write('Avec ce graphique, on peut voir que les Assassins et les Fighters ont généralement moins de Mana que les autres. La ou les Mages en ont plus que les autres.\n On peut donc determiner qu’un champion qui a moins de Mana à de grande chance d’être un Fighter ou un Assassin et dans le cas contraire, il sera certainement un Mage')

st.markdown("### 5) L'Attaque")

sb.relplot(df, x='Primary_Role', y='Attack_Demage', kind='line')

st.pyplot(plt.gcf())

st.write('Avec ce graphique. On peut voir que les Mages et les Supports ont moins de dégats (Attack_Demage) que les autres. En se basant sur les autres graphiques et celui-ci, on peut donc en conclure une chose :\n Si un champion à assez de Mana, assez peu de portée et peu de dégât, alors il sera Support')

st.markdown('### 6) Dégâts par Niveau')

sb.boxplot(df, x='Primary_Role', y='Health_Regen', showfliers = False)

st.pyplot(plt.gcf())

st.write("Sur ce graphique, on peut determiner que les Marksmans ont beaucoup moins régénération de vie que les autres au niveau 1. Ce qui nous permet de les différencier encore plus des autres.")

st.write("Conclusion\nAvec ces différents graphiques, j’ai pu determiner plusieurs choses compte tenu du dataset à ma disposition : \n Si un champion a assez de Mana, assez peu de porté et peu de dégâts, il a des chances d’être Support\nSi un champion a beaucoup de Mana et peu d’armure, il sera certainement un Mage\nSi un champion a peu d’armure et une grande portée, il sera certainement un Tireur\nPour ce qui est des Assassins, des Combattants ou des Tanks. Pour moi les différences ne sont pas assez notables pour les différencier avec leurs statistiques au niveau 1\nDonc pour répondre à ma problématique. Oui, il est possible de déterminé le rôle d’un champion sur ses statistiques au niveau 1. Et avec des données en plus je pourrais donc affiner les résultats que j’ai déjà pu avoir avec ce jeu de données")